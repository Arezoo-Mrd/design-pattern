import Flyweight from './../flyweight.mdx'

const FlyweightPattern = () => {
  return (
    <div className='flex flex-col items-center justify-center text-xl font-bold text-center text-white'>
      <Flyweight />
    </div>
  )
}

export default FlyweightPattern