class XmlDB{
  getData (){
    return 'XML DATA'
  }
}


class Chart {
    showResult(json) {
        console.log('%c Json','background: #FFF; color: #000;padding: 0.25rem;border-radius: 5px',JSON.stringify(json));
    }
}


class XmlToJsonAdapter {
    constructor(xml) {
        this.xml = xml
    }
    convert () {
        console.log('%c xml','background: #FFF; color: #000;padding: 0.25rem;border-radius: 5px',this.xml);
        return {

            name: "Arzeoo Moradi"
        }
    }
}



const xmlDB = new XmlDB()
const data  = xmlDB.getData()
const adapter = new XmlToJsonAdapter()
const convertData = adapter.convert(data)
const chart = new Chart()
chart.showResult(convertData)