class FlyweightBook {
    constructor(name = name, author=author, isbn = isbn){}
}


class BookFactory {
  constructor() {
    // this.count = 0
    this.flyweightBooks = {}
  }
  createBook(name, author, isbn){
    var book = this.flyweightBooks[isbn]
    console.log("🚀 ~ file: flyweight.js:23 ~ BookFactory ~ createBook ~ book:", book)
    if(book) {
      return book
    } else {
      var newBook = new FlyweightBook(name, author, isbn)
      this.flyweightBooks[isbn] = newBook
      return newBook
    }
  }
 
}

class BookContext {
    constructor(bookFactory) {
      this.count = 0 
      this.bookFactory = bookFactory
      this.bookList = {}
    }
    add(name, author, isbn , availability, sales) {
      var book = this.bookFactory.createBook(name, author, isbn)
      book.sales = sales
      book.availability = availability
      this.bookList[isbn] = book
      this.count++;
    }
    getCount() {
      return this.count
    }
} 

var BOOK_FACTORY = new BookFactory();
console.log("🚀 ~ file: flyweight.js:58 ~ BOOK_FACTORY:", BOOK_FACTORY)
var BOOK_CONTEXT = new BookContext(BOOK_FACTORY);
console.log("🚀 ~ file: flyweight.js:60 ~ BOOK_CONTEXT:", BOOK_CONTEXT)
BOOK_CONTEXT.add('you dont know js', 'kyle simpson','FGB45',true,50)
// BOOK_CONTEXT.add('you dont know js', 'kyle simpson','FGB45',true,50)
console.log('%c BOOK_CONTEXT.getCount()','background: #FFF; color: #000;padding: 0.25rem;border-radius: 5px',BOOK_CONTEXT.getCount());