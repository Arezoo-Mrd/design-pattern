import SyntaxHighlighter from 'react-syntax-highlighter'
import HelloWorld from './hello.mdx'
import Link from 'next/link'
 
export default function Page() {

  return (
    <div className='flex items-center justify-center w-full h-screen text-lg font-bold'>
      <div className='p-5'>
        <Link href="./flyweight">
    <div className='w-[250px] h-[250px] flex items-center justify-center p-5 shadow-2xl bg-white rounded-xl'>
      Flyweight pattern
    </div>
    </Link>
      </div>
            <div className='p-5 '>
        <Link href="./decorator">
    <div  className='w-[250px] h-[250px] flex items-center justify-center p-5 shadow-2xl bg-white rounded-xl'>
      Decorator pattern
    </div>
    </Link>
      </div>

    </div>
  )
  
}